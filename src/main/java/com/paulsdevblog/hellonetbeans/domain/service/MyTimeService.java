
package com.paulsdevblog.hellonetbeans.domain.service;

import com.paulsdevblog.hellonetbeans.domain.model.MyTime;

/**
 * Simple service interface to MyTime
 *
 * @author paulsdevblog.com
 */
public interface MyTimeService {

    MyTime getMyTime();
    
}
