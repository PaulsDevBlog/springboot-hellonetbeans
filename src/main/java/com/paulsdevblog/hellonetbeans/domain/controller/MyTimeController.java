
package com.paulsdevblog.hellonetbeans.domain.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paulsdevblog.hellonetbeans.domain.service.MyTimeService;
import com.paulsdevblog.hellonetbeans.domain.model.MyTime;

/**
 * Simple REST controller example that uses MyTimeService
 *
 * @author paulsdevblog.com
 */

@RestController
@RequestMapping("/time")
public class MyTimeController {
    
    private Logger logger = LoggerFactory.getLogger(MyTimeController.class );
    
    @Autowired
    private MyTimeService myTimeService;
    
    @RequestMapping(
        value={"/current" }, 
        method = RequestMethod.GET,
        produces = { MediaType.APPLICATION_JSON_VALUE }
    )
    public ResponseEntity<MyTime> currentTime(){
        
        MyTime currentMyTime = myTimeService.getMyTime();
        
        return new ResponseEntity<MyTime>( currentMyTime, HttpStatus.OK );
        
    }
    
}
