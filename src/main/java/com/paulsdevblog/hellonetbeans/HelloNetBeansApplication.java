package com.paulsdevblog.hellonetbeans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class HelloNetBeansApplication {
    
    public static final Logger logger = LoggerFactory.getLogger( HelloNetBeansApplication.class );

    public static void main(String[] args) {
        
        SpringApplication.run(HelloNetBeansApplication.class, args);
        
        logger.info("--Application Started--");
        logger.info("This is our Hello NetBeans and Hello World Micro-Service!");
    }
}
